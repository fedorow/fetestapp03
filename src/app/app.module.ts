import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from '@app/app.component';
import {RoutingModule} from '@app/modules/routing.module';
import {CoreModule} from '@app/modules/core/core.module';
import {SharedModule} from '@app/modules/shared/shared.module';
import {BootstrapModule} from '@app/modules/bootstrap.module';
import {PagesModule} from '@app/modules/pages/pages.module';


@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		RoutingModule,
		CoreModule,
		SharedModule,
		PagesModule,
		BootstrapModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
