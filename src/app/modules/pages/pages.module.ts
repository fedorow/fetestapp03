import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WorkplaceComponent} from '@app/modules/pages/workplace/workplace.component';
import {DetailsComponent} from '@app/modules/pages/details/details.component';
import {NotFoundComponent} from '@app/modules/pages/not-found/not-found.component';
import {SharedModule} from '@app/modules/shared/shared.module';
import {BootstrapModule} from '@app/modules/bootstrap.module';
import {FormsModule} from '@angular/forms';

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		BootstrapModule,
		FormsModule
	],
	declarations: [
		WorkplaceComponent,
		DetailsComponent,
		NotFoundComponent
	]
})
export class PagesModule {
}
