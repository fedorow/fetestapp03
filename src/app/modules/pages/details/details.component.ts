import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Order} from '@app/models/Order';
import {WebSocketService} from '@app/modules/core/services/web-socket.service';

@Component({
	selector: 'u-details',
	templateUrl: './details.component.html',
	styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

	public order: Order;

	public get rating() {
		return this.order ? this.order.rating : 0;
	}

	public set rating(r: number) {
		if (this.order) {
			this.order.rating = r;
		}
	}

	public isRateChanged: boolean;

	constructor(private route: ActivatedRoute,
				private router: Router,
				private wsService: WebSocketService) {
	}

	ngOnInit() {
		const id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
		this.order = this.wsService.getOrder(id);
		this.isRateChanged = false;

		if (!this.order) {
			this.router.navigate(['workplace']);
		}
	}

	onRatingClick() {
		this.isRateChanged = true;
	}

	onSubmitClick() {
		if (this.isRateChanged) {
			this.wsService.rateOrder(this.order);
			this.isRateChanged = false;
		}

		this.router.navigate(['workplace']);
	}
}
