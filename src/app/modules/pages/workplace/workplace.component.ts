import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Order} from '@app/models/Order';
import {WebSocketService} from '@app/modules/core/services/web-socket.service';

@Component({
	selector: 'u-workplace',
	templateUrl: './workplace.component.html',
	styleUrls: ['./workplace.component.scss']
})
export class WorkplaceComponent implements OnInit, OnDestroy {

	public static currentPage = 1;

	public currentPage = WorkplaceComponent.currentPage;
	public itemsPerPage = 10;

	public get pageOrders(): Array<Order> {
		return this.wsService.getAllOrders().splice((this.currentPage - 1) * this.itemsPerPage, this.itemsPerPage);
	}

	constructor(public wsService: WebSocketService,
				private router: Router) {
	}

	ngOnInit() {
	}

	ngOnDestroy(): void {
		WorkplaceComponent.currentPage = this.currentPage;
	}

	onOrderClose(order: Order) {
		this.wsService.closeOrder(order);
	}

	onOrderDetails(order: Order) {
		this.router.navigate([`details/${order.order_id}`]);
	}

	onOrderRating(order: Order) {
		this.wsService.rateOrder(order);
	}

}
