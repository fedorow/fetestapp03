import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DetailsComponent} from '@app/modules/pages/details/details.component';
import {WorkplaceComponent} from '@app/modules/pages/workplace/workplace.component';
import {NotFoundComponent} from '@app/modules/pages/not-found/not-found.component';

const routes: Routes = [
	{path: '', pathMatch: 'full', redirectTo: 'workplace'},
	{path: 'workplace', component: WorkplaceComponent},
	{path: 'details', component: DetailsComponent},
	{path: 'details/:id', component: DetailsComponent},
	{path: '**', component: NotFoundComponent}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class RoutingModule {
}
