import {NgModule} from '@angular/core';
import {AlertModule, ButtonsModule, PaginationModule, RatingModule} from 'ngx-bootstrap';

@NgModule({
	imports: [
		AlertModule.forRoot(),
		ButtonsModule.forRoot(),
		PaginationModule.forRoot(),
		RatingModule.forRoot()
	],
	exports: [
		AlertModule,
		ButtonsModule,
		PaginationModule,
		RatingModule
	]
})
export class BootstrapModule {
}
