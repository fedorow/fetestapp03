import {Injectable} from '@angular/core';
import {WebSocketSubject} from 'rxjs/observable/dom/WebSocketSubject';
import {Subscription} from 'rxjs/Subscription';
import {Order} from '@app/models/Order';
import 'rxjs/add/operator/shareReplay';
import {environment} from '@env/environment';

@Injectable()
export class WebSocketService {

	private orders: Array<Order>;

	private socket: WebSocketSubject<Order>;
	private subscription: Subscription;

	constructor() {
		this.orders = [];
	}

	/**
	 * Connect to WS
	 * @returns {WebSocketSubject<Order>}
	 */
	public start(): WebSocketSubject<Order> {
		this.socket = WebSocketSubject.create(environment.wsUrl);
		this.subscription = this.socket
			.shareReplay()
			.subscribe(
				data => {
					// handle different actions from server
					switch (data['action']) {
						case 'rate': {
							const order = this.orders
								.find(o => o.order_id === data.order_id);
							order.rating = data.rating;
							break;
						}
						case 'close': {
							const order = this.orders
								.find(o => o.order_id === data.order_id);
							this.orders.splice(this.orders.indexOf(order), 1);
							break;
						}
						default: {
							const order = new Order();
							order.deserialize(data);
							this.orders.unshift(order);
						}
					}
				},
				() => {
				},
				() => {
				}
			);

		return this.socket;
	}

	/**
	 * Close connection and remove all existing orders
	 */
	public stop(): void {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}

		if (this.socket) {
			this.socket.complete();
		}

		this.orders = [];
	}

	/**
	 * Get copy of array with all existing orders
	 * @returns {Array<Order>}
	 */
	public getAllOrders(): Array<Order> {
		return [...this.orders];
	}

	/**
	 * Get concrete Order by Id
	 * @param {number} id
	 * @returns {Order}
	 */
	public getOrder(id: number): Order {
		return this.orders.find(o => o.order_id === id);
	}

	/**
	 * Send action to close existing route
	 * @param {Order} order
	 */
	public closeOrder(order: Order) {
		this.socket.next(<any>JSON.stringify({
			action: 'close',
			order_id: order.order_id
		}));
	}

	/**
	 * Send action to change rating of existing order
	 * @param {Order} order
	 */
	public rateOrder(order: Order) {
		this.socket.next(<any>JSON.stringify({
			action: 'rate',
			order_id: order.order_id,
			rating: order.rating
		}));
	}

}
