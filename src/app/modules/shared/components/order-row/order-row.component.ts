import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Order} from '@app/models/Order';

@Component({
	selector: 'u-order-row',
	templateUrl: './order-row.component.html',
	styleUrls: ['./order-row.component.scss']
})
export class OrderRowComponent implements OnInit {

	@Input()
	public order: Order;

	@Output()
	public close = new EventEmitter<Order>();

	@Output()
	public details = new EventEmitter<Order>();

	@Output()
	public rating = new EventEmitter<Order>();

	constructor() {
	}

	ngOnInit() {
	}

	onCloseClick() {
		this.close.emit(this.order);
	}

	onDetailsClick() {
		this.details.emit(this.order);
	}

	onRatingClick() {
		this.rating.emit(this.order);
	}
}
