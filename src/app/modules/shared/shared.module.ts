import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OrderRowComponent} from '@app/modules/shared/components/order-row/order-row.component';
import {FormsModule} from '@angular/forms';
import {BootstrapModule} from '@app/modules/bootstrap.module';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		BootstrapModule
	],
	declarations: [
		OrderRowComponent
	],
	exports: [
		OrderRowComponent
	]
})
export class SharedModule {
}
