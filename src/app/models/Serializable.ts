export class Serializable {

	public deserialize(json: Object): Object {
		Object.assign(this, json);
		return this;
	}

}
