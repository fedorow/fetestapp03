import {Serializable} from '@app/models/Serializable';

export class Order extends Serializable {

	public order_id: number;
	public driver_phone: string;
	public pass_phone: string;
	public rating: number;

}
