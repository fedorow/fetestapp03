import {Component, OnDestroy, OnInit} from '@angular/core';
import {WebSocketService} from '@app/modules/core/services/web-socket.service';
import {Subscription} from 'rxjs/Subscription';
import {Router} from '@angular/router';

@Component({
	selector: 'u-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

	public title = 'Taxi';
	public errorAlert = '';

	private subscription: Subscription;

	constructor(private wsService: WebSocketService,
				private router: Router) {
	}

	ngOnInit(): void {
		this.subscription = this.wsService.start()
			.subscribe(
				() => {
				},
				err => {
					// Show alert if WS connection has been lost
					this.errorAlert = 'Connection has been lost. Try refresh the app!';
					console.error(err);
				},
				() => {
				}
			);
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
		this.wsService.stop();
	}

	onTitleClick() {
		this.router.navigate(['workplace']);
	}
}
