import * as express from 'express';
import {Express} from 'express';
import * as http from 'http';
import * as WebSocket from 'ws';
import {Order} from './Order';
import Timer = NodeJS.Timer;

export class WSServer {

	private readonly app: Express;
	private readonly httpServer: http.Server;
	private readonly wsServer: WebSocket.Server;

	private timerId: Timer;
	private orders: Array<Order>;

	constructor() {
		this.app = express();

		this.httpServer = http.createServer(this.app);
		this.app.get('/', (req, res) => {
			res.send(`Server is working. Use ws:// protocol`);
		});

		this.wsServer = new WebSocket.Server({server: this.httpServer});
		this.wsServer.on('connection', (ws: WebSocket) => {
			ws.on('message', (message: string) => {
				const command = JSON.parse(message);

				const order = this.orders
					.find(o => o.order_id === command.order_id);

				if (order) {
					// handle different actions from client
					switch (command.action) {
						case 'rate': {
							order.rating = command.rating;
							break;
						}
						case 'close': {
							this.orders.splice(this.orders.indexOf(order), 1);
							break;
						}
					}

					this.updateOrder(order, command.action);
				}
			});

			// send all existing orders to new client
			this.orders
				.forEach(o => this.sendOrder(ws, o));
		});
	}

	/**
	 * Start creating of new orders
	 * @param {number} port
	 * @param {number} interval
	 */
	public start(port: number, interval: number): void {
		this.orders = [];

		this.httpServer.listen(port, () => {
			console.log(`Server started on port ${this.httpServer.address().port}`);
		});

		this.timerId = setInterval(() => this.addOrder(), interval);

		this.addOrder();
	}

	/**
	 * Stop creating of new orders and remove all existing orders
	 */
	public stop(): void {
		clearInterval(this.timerId);

		this.orders = [];
	}

	/**
	 * Add new order to array notify all clients
	 */
	private addOrder() {
		const order = Order.create();
		this.orders.push(order);

		this.wsServer.clients
			.forEach(ws => this.sendOrder(ws, order));
	}

	/**
	 * Update existing order and notify all clients
	 * @param {Order} order
	 */
	private updateOrder(order: Order, action: string) {
		order.action = action;

		this.wsServer.clients
			.forEach(ws => this.sendOrder(ws, order));

		order.action = '';
	}

	/**
	 * Send order to client
	 * @param {WebSocket} ws
	 * @param {Order} order
	 */
	private sendOrder(ws: WebSocket, order: Order): void {
		const str = JSON.stringify(order);

		if (ws.readyState === 1) {
			ws.send(str);
		}
	}
}
