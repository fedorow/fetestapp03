export class Order {

	private static COUNTRY_CODE = '38';
	private static OPERATOR_CODES = ['050', '067', '068', '096', '097', '098'];

	private static order_id = 0;


	public readonly order_id: number;
	public driver_phone: string;
	public pass_phone: string;
	public rating: number;
	public action: string;

	constructor(driver_phone: string, pass_phone: string, rating: number = 5) {
		this.order_id = ++Order.order_id;
		this.driver_phone = driver_phone;
		this.pass_phone = pass_phone;
		this.rating = rating;
		this.action = '';
	}


	/**
	 * Create Mock Order
	 * @returns {Order}
	 */
	public static create(): Order {
		const driver_phone = Order.getRandomPhone();
		const pass_phone = Order.getRandomPhone();
		const rating = Math.round(Math.random() * 5);

		return new Order(driver_phone, pass_phone, rating);
	}

	/**
	 * Generate Random Phone Number
	 * @returns {string}
	 */
	private static getRandomPhone(): string {
		const code = Order.OPERATOR_CODES[Math.floor(Math.random() * Order.OPERATOR_CODES.length)];
		const num = new Array(7)
			.fill(0)
			.map(v => Math.round(Math.random() * 9))
			.join('');

		return `+${Order.COUNTRY_CODE} (${code}) ${num}`;
	}
}
