import {WSServer} from './WSServer';

const INTERVAL_IN_SECONDS = 10;
const PORT = process.env.PORT || 8999;

const server = new WSServer();
server.start(PORT, INTERVAL_IN_SECONDS * 1000);
